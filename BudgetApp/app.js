// DATA MODULE::BUDGET CONTROLLER
var budgetController = ( function () {

    var Expense = function(id,description,value){
        this.id = id;
        this.description = description;
        this.value = value;
    }

    var Income = function(id,description,value){
        this.id = id;
        this.description = description;
        this.value = value;
    }

    var calculateIncomeAndExpenses = function (type,value) {
        data.total[type] = data.total[type] + value;
    }
    var calculateBudget  = function () {
        data.budget = data.total.inc - data.total.exp;
    }
    var calculatePercentage = function (){
        if(data.total.inc > 0) {
            data.percentage = Math.round (data.total.exp/(data.total.inc*0.01));
        } else {
            data.percentage = -1;
        }
    }
    var data = {
        allItems : {
            exp : [],
            inc : []
        },
        total : {
            exp : 0,
            inc : 0
        },
        budget : 0,
        percentage : -1
    }

    return {
        addItem : function (type,description,value) {
            var newItem,ID;
            //Create new ID
            if(data.allItems[type].length > 0) {
                ID = data.allItems[type][data.allItems[type].length-1].id + 1;
            } else {
                ID = 0;
            }

            //Create new Item based on 'inc' of 'exp'
            if(type === 'exp'){
                newItem = new Expense(ID,description,value);
            } else if (type === 'inc'){
                newItem = new Income(ID,description,value);
            }
            //Updating the Data arrays by adding the new item and increasing the proper total(exp of inc)
            data.allItems[type].push(newItem);
            calculateIncomeAndExpenses(type, newItem.value);

            // Returning the last submitted item so that it may be used outside this controller
            return newItem;
        },
        calculateTotal : function (){
            //Calculate budget
            calculateBudget();

            //Calculate percentage
            calculatePercentage();
        },

        testing : function () {
            console.log(data);
        },
        getExpensePercentage :function (item) {
            return item.value/(this.getTotal('inc')*0.01);
        },
        getTotals: function (type) {
            return data.total[type];
        },
        getBudget : function () {
            return {
                budget : data.budget,
                incomeTotal : data.total.inc,
                expensesTotal : data.total.exp,
                percentage : data.percentage   
            }
        },
        
    }
   
})();

// UI MODULE :: UI CONTROLLER
var UIController = ( function () {
    var DOMStrings = {
        inputType :'.add__type',
        inputDescription :'.add__description',
        inputValue : '.add__value',
        inputButton :'.add__btn',
        incomeContainer : '.income__list',
        expensesContainer : '.expenses__list',
        incomeTotal : '.budget__income--value',
        expensesTotal :'.budget__expenses--value',
        expensesPercentage :'.budget__expenses--percentage',
        itemExpPrecentage : '.item__percentage',
        budgetTotal : '.budget__value',
        budgetMonth : '.budget__title--month',
        deleteItemButton :'.item__delete--btn'
    }
    return {
        getInput: function(){
            return {
                // 'entryType' will be inc for income, or exp for expenses
                type : document.querySelector(DOMStrings.inputType).value,
                description : document.querySelector(DOMStrings.inputDescription).value,
                value : parseFloat(document.querySelector(DOMStrings.inputValue).value)
            }
        },
        addListItem : function (obj,type) {
            var html, newHtml, element;
            //Creates Html string with placeholder text
            if (type === 'inc') {
                element = DOMStrings.incomeContainer;
                html = '<div class="item clearfix" id="income-%id%">'+
                            '<div class="item__description">%description%</div>'+
                            '<div class="right clearfix">'+
                                '<div class="item__value">%value%</div>'+
                                '<div class="item__delete">'+
                                    '<button class="item__delete--btn" value="inc%id%"><i class="ion-ios-close-outline"></i></button>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
            } else if( type === 'exp') {
                element = DOMStrings.expensesContainer;
                html = '<div class="item clearfix" id="expense-%id%">'+
                            '<div class="item__description">%description%</div>'+
                            '<div class="right clearfix">'+
                                '<div class="item__value">%value%</div>'+
                                '<div class="item__percentage">%percentage%</div>'+
                                '<div class="item__delete">'+
                                    '<button class="item__delete--btn" value="exp%id%"><i class="ion-ios-close-outline"></i></button>'+
                                '</div>'+
                            '</div>'+
                       '</div>';
            }
            //Replace  placeholder text with actual data
                newHtml = html.replace('%id%',obj.id);
                newHtml = newHtml.replace('%id%',obj.id);
                newHtml = newHtml.replace('%description%',obj.description);
                newHtml = newHtml.replace('%value%',obj.value);

            //Inserrt Html into the DOM
                document.querySelector(element).insertAdjacentHTML('beforeend', newHtml);
        },
        
        updateBudgetUI : function (obj) {
            var sign;
            obj.incomeTotal > 0 ? sign = '+' :  sign = '';
            document.querySelector(DOMStrings.incomeTotal).textContent = sign + obj.incomeTotal;
            obj.expensesTotal > 0 ? sign = '-' :  sign = '';
            document.querySelector(DOMStrings.expensesTotal).textContent = sign + obj.expensesTotal;
        
            if( obj.percentage !== -1) {
                document.querySelector(DOMStrings.expensesPercentage).textContent = obj.percentage + '%';
            } else {
                document.querySelector(DOMStrings.expensesPercentage).innerHTML = '...  ';
            }
            obj.budget > 0 ? sign = '+' :  sign = '';
            document.querySelector(DOMStrings.budgetTotal).textContent = sign + obj.budget;
        },
        udateMonth : function (){
            var date = new Date();
            var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            document.querySelector(DOMStrings.budgetMonth).innerHTML = '<strong>' + months[date.getMonth()] + '</strong>';
        },
        clearFields : function (){
            var fields, fieldsArray;
            fields = document.querySelectorAll(DOMStrings.inputDescription + ', ' +DOMStrings.inputValue);
            fieldsArray = Array.prototype.slice.call(fields);
            fieldsArray.forEach( function (current){
                current.value = '';
            });
            fieldsArray[0].focus();
        },
        getDOMStrings : function (){
            return DOMStrings;
        }

    }
})();

// APP CONTROLLER 
var controller = ( function (budgetCtrl,UICtrl) {

    //Initialize the events and DOM manipulations, to be called by the public init() function
    var setupEventListeners = function(){
        var DOM = UICtrl.getDOMStrings();
        if(document.querySelector(DOM.deleteItemButton)){
            console.log('Here I am');
            document.querySelector(DOM.deleteItemButton).addEventListener('click', function(event){
                console.log(event.target.value);
            });
        }
        /* var btnString = document.querySelector(DOMStrings.deleteItemButton).value;
        var elementType = btnString.substr(0,3);
        var elementId  = btnStr.substr(3,1);
        console.log(elementType + " - " + elementId); */
        document.querySelector (DOM.inputButton).addEventListener('click', ctrlAddItem);

        document.addEventListener('keypress', function (event) {
            if(event.keyCode === 13){
                ctrlAddItem();
            }
        });
    }
    var updateBudget = function (type,obj) {
        var budget;
        // 1. Calculate the budget// I don't now what I am doing here, or why... 
        obj.calculateTotal();

        // 2. Return the budget
        budget = obj.getBudget();

        // 3. Display the budget on UI
        UICtrl.updateBudgetUI(budget);
    }
    var ctrlAddItem = function (){
        var input, newItem, expTotal, incTotal,expPercentage,budgetTotal;
        // 1. Get input fields data 
        input = UICtrl.getInput();

        if((input.description !== "") && (!isNaN(input.value)) && (input.value > 0)){
            // 2. Add item to budget Controller 
            newItem = budgetCtrl.addItem(input.type,input.description,input.value);
            // 3. Add te item to UI as well
            UICtrl.addListItem(newItem,input.type);
    
            // 4. Clear the input fields
            UICtrl.clearFields();
    
            // 5. Calculate and Update budget
            updateBudget(input.type, budgetCtrl);
        }
    }
    //creating and returning public init() function
    return {
        init: function (){
            console.log('Start');
            UICtrl.udateMonth();
            UICtrl.updateBudgetUI({
                budget : 0,
                incomeTotal : 0,
                expensesTotal : 0,
                percentage : -1   
            });
            setupEventListeners();
        }
    }
})(budgetController,UIController);

//Calling the init() function of the controller
controller.init();