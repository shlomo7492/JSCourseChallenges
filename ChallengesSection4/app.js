/*
GAME RULES:

- The game has 2 players, playing in rounds
- In each turn, a player rolls a dice as many times as he whishes. Each result get added to his ROUND score
- BUT, if the player rolls a 1, all his ROUND score gets lost. After that, it's the next player's turn
- The player can choose to 'Hold', which means that his ROUND score gets added to his GLBAL score. After that, it's the next player's turn
- The first player to reach 100 points on GLOBAL score wins the game

*/

var scores, roundScore, activePlayer, dice, diceTwo, gameMode, winCondition, diceDOM,  diceArray;

diceDOM  = document.querySelector('.dice');
diceTwoDOM  = document.querySelector('.dice-two');
function diceToggle (toggle,toggleTwo) {
    diceDOM.style.display = toggle;
    diceTwoDOM.style.display = toggleTwo;
}

init();


// Initialize the game data in the initial load and on 'New Game' button click
function init(){
    
    scores = [0,0];
    roundScore = 0;
    activePlayer = 0; 
    gameMode = 1;
    console.log(gameMode);
    winCondition = 100;
    diceArray = [0,0];
    diceToggle('none','none');
    document.querySelector('.mode').textContent ='Two dices';
    document.getElementById('win-condition').value = 100;
    document.getElementById('game-mode').style.display = "block";
    document.getElementById('replacement').style.display = "none";
    document.getElementById('win-condition').style.display = "inline";
    document.getElementById('victory-condition').style.display = "none";
    document.querySelector('.btn-hold').style.display='block';
    document.querySelector('.btn-roll').style.display='block';
    document.getElementById('score-0').textContent=scores [0];
    document.getElementById('score-1').textContent=scores [1];
    document.getElementById('current-0').textContent='0';
    document.getElementById('current-1').textContent='0';
    document.getElementById('score-1').textContent='0'; 
    document.getElementById('name-0').textContent='Player 1';  
    document.getElementById('name-1').textContent='Player 2';  
    document.querySelector('.player-0-panel ').classList.remove('winner');
    document.querySelector('.player-1-panel ').classList.remove('winner'); 
    document.querySelector('.player-0-panel ').classList.remove('active');
    document.querySelector('.player-1-panel ').classList.remove('active'); 
    document.querySelector('.player-0-panel ').classList.add('active'); 
}


// Adding some real action to the buttons 'New Game', 'Roll Dice' and 'Hold'
document.querySelector('.btn-new').addEventListener('click',init);
document.querySelector('.btn-roll').addEventListener('click', function() {
    //disable game mode button by pointing it to duplicate class
    // Disable the game mode button when game is started
    document.getElementById('game-mode').style.display = "none";
    document.getElementById('replacement').style.display = "block";
    document.getElementById('win-condition').style.display = "none";
    document.getElementById('victory-condition').textContent = winCondition;
    document.getElementById('victory-condition').style.display = "inline";
    diceToggle('none','none');
    //Roll the dice (dices) 
    if(gameMode === 1){
        rollOne();
    } else {
        rollTwo();
    }
});

//roll button action in one dice game mode
function rollOne(){
    // Apply Random number (from 1 to 6) to dice variable
    dice = Math.floor(Math.random() * 6) + 1;

    //Displaying the Dice for it was hidden by default
    diceToggle('block','none');
    diceDOM.src = 'dice-'+dice+'.png';

    //keeps the last two dice rolls
    diceArray.shift();
    diceArray.push(dice);

    //checks if the sum of the last two rolls is 12 and if so sets the score
    // and current score of the current player to 0 otherwise continue with the rolling
    if ((diceArray[0]+diceArray[1]) === 12){
        scores [activePlayer] = 0;
        document.getElementById('score-' + activePlayer).textContent = 0;
        diceToggle('none','none');
        changePlayer();
    } else{
        // Update update the current score for the active player in case it rolls !1
        if (dice !== 1){
            roundScore += dice;
            document.getElementById('current-' + activePlayer).textContent = roundScore;
        } else {
            diceToggle('none','none');
            changePlayer();
        }
    }
};

//roll button action in two dices game mode
function rollTwo(){
    // Apply Random number (from 1 to 6) to dice and diceTwo variables
    dice = Math.floor(Math.random() * 6) + 1;
    diceTwo = Math.floor(Math.random() * 6) + 1;

    //Displaying the Dice for it was hidden by default
    diceToggle('block','block');
    diceDOM.src = 'dice-'+dice+'.png';
    diceTwoDOM.src = 'dice-'+diceTwo+'.png';

    // Update update the current score for the active player in case it rolls !1
    if ((dice !== 1)&&(diceTwo !== 1)) {
        roundScore += (dice + diceTwo);
        document.getElementById('current-' + activePlayer).textContent = roundScore;
    } else {
        diceToggle('none','none');
        changePlayer();
    }
};
//Adds current score to the players score NOTE: This works in all game modes
document.querySelector('.btn-hold').addEventListener('click', function() {
    if(scores[activePlayer] < winCondition){
        scores[activePlayer] += roundScore;
        document.getElementById('score-0').textContent=scores[0];
        document.getElementById('score-1').textContent=scores[1];
        diceToggle('none','none');
        if(scores[activePlayer] >= winCondition){
            document.getElementById('name-' + activePlayer).textContent='Winner';
            document.querySelector('.player-'+activePlayer+'-panel').classList.add('winner');
            document.querySelector('.player-'+activePlayer+'-panel').classList.remove('active');
            document.querySelector('.btn-hold').style.display='none';
            document.querySelector('.btn-roll').style.display='none';
        } else {
            changePlayer();
        }
    }
    
});

function changePlayer() {
    diceArray = [0,0];
    roundScore = 0;
    document.getElementById('current-' + activePlayer).textContent = '0';
    document.querySelector('.player-' + activePlayer +'-panel').classList.toggle('active');
    activePlayer === 0 ? activePlayer = 1 : activePlayer = 0;
    document.querySelector('.player-' + activePlayer +'-panel').classList.toggle('active');
}
// switches between One dice and Two dices game modes
document.getElementById('game-mode').addEventListener('click', function(){
    if (gameMode === 1) {
         gameMode = 2;
         document.querySelector('.mode').textContent ='One dice';
    } else{
        gameMode = 1;
        document.querySelector('.mode').textContent ='Two dices';
    }   
    console.log(gameMode);
} );
// Set/change win condition
document.getElementById('Set-win-condition').addEventListener('change', function (){
    winCondition = document.getElementById('win-condition').value;
    console.log(winCondition);
});