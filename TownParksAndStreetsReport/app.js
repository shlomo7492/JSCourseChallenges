class TownElement {
    constructor (name,yearOfBuild){
        this.name = name;
        this.yearOfBuild = yearOfBuild;
    }

    getAge(){
        let yearNow = new Date ().getFullYear();
        return yearNow - this.yearOfBuild;
    }
}

class Park extends TownElement{
    constructor (name, yearOfBuild, trees, area ) {
        super (name,yearOfBuild)
        this.trees = trees;
        this.area = area;
    }

    getParkTreesDensity () {
        return this.trees/(this.area/1000000);
    }
    printTousandTreeMessage () {
        if(this.trees > 1000) {
            console.log(`${this.name} has more than 1000 trees.`);
        }
    }
}

class Street extends TownElement{
    constructor (name, yearOfBuild, streetLength, streetSize = 'normal' ) {
        super (name,yearOfBuild)
        this.streetLength = streetLength;
        this.streetSize = streetSize;
    }
    printStreetInfo (){
        console.log(`${this.name} , build in ${this.yearOfBuild}, is a ${this.streetSize} street.`)
    }
}

//defining and initializing the collections of parks and streets

var townParks = new Map();
townParks.set('City Garden',new Park('City garden',1878,75,22500));
townParks.set ('King Simeon\'s garden',new Park ('King Simon\'s Garden', 1892,350,90000));
townParks.set ('Lauta garden', new Park ('Lauta Park', 1981,1670,420000));

var townStreets = new Map();
townStreets.set('bul. "Bulgaria"', new Street('bul. "Bulgaria"', 1969, 13.450, 'huge'));
townStreets.set('bul. "Tsarigradsko shose"', new Street('bul. "Tsarigradsko shose"', 1981, 5.1, 'big'));
townStreets.set('ul. "Gladston"',new Street('ul. "Gladston"', 1923, 2));
townStreets.set('ul. "Jeravna"',new Street('ul. "Jeravna"', 1975, 0.5,'tiny'));

//Parks Reports
//Returns the average park age
var averageParkAgeReport = function (parks) {
    var total = 0;
    //Sums the ages of the parks
    parks.forEach((value,key) =>
        total += value.getAge()
    );
    console.log(`Our ${parks.size} parks have an average age of ${total/parks.size} years.`);
}
//Prints on each line the parks tree density per square km.
var parksDensityReport = function (parks) {
    parks.forEach((value,key) => 
        console.log(`${key} has a tree  density of ${value.getParkTreesDensity()} trees per square km.`)
    );
}
//Prints parks with more than 1000 trees
var parksMoreThanTousandReport = function (parks) {
    var moreThanTousand  = [];
    parks.forEach((value,key) =>  
        value.printTousandTreeMessage()
    );
}
//Streets Reports
//Prints Total and Average length of all  Town streets
var totalAndAverageTownStreetsLength = function (streets) {
    var total = 0;
    //Sums the ages of the parks
    streets.forEach((value,key) =>
        total += value.streetLength
    );
    console.log(`Our ${streets.size} streets have total length of ${total} km, with an average of ${total/streets.size} km.`);
}
//
var printStreetsSizes = function (streets) {
    streets.forEach((value) =>
        value.printStreetInfo()
    )
}
//FINAL REPORT
//Prints the entire report to the mayor
var reportToTheMayor = function (parks,streets) {
    console.log('---- PARKS REPORT ----');
    averageParkAgeReport(parks);
    parksDensityReport(parks);
    parksMoreThanTousandReport(parks);
    console.log('---- STREETS REPORT ----');
    totalAndAverageTownStreetsLength(streets);
    printStreetsSizes(streets);
}

reportToTheMayor(townParks,townStreets);