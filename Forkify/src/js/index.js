// Global app controller
import  Search from './models/Search';
import  Recipe from './models/Recipe';
import * as searchView from './views/searchView';
import * as recipeView from './views/recipeView';

import { elements, renderLoader, clearLoader } from './views/base';
/** Global state of the app
* - Search object
* - Current recipe object
* - Shopping list obejct
* - Liked recepies
*/

const state ={};
/*
 *   SEARCH CONTROLLER
 */
const controlSearch = async () => {
    //get query from the view
    const query = searchView.getInput(); 

    if (query) {
        // new search object and add to state
        state.search = new Search(query);

        // Prepare UI for results
        searchView.clearInput();
        searchView.clearResults();
        renderLoader(elements.searchResultsContainer);

        try {
            // Search for recepies
            await state.search.getResult();
            //render search results on UI
            clearLoader();
            searchView.renderSearchResults(state.search.result);

        } catch (err) {
            //alert('Something wrong with the search...');
            console.log(err);
            clearLoader();
        }
    }
}

elements.searchForm.addEventListener ('submit', e =>{
    e.preventDefault();
    controlSearch();
});
var app =''

elements.searchResultsPages.addEventListener('click', e =>{
    const btn = e.target.closest('.btn-inline');
    if(btn) {
        const goToPage = parseInt(btn.dataset.goto, 10);
        searchView.clearResults();
        searchView.renderSearchResults(state.search.result,goToPage);
    }
})

/*
 *   RECIPE CONTROLLER
 */
const  controlRecipe = async () => {
   // Adding Recipe to the state variable
   const id = window.location.hash.replace('#','');
   console.log(id);
   if(id){
       //Prepare UI for changes
        recipeView.clearRecipe();
        renderLoader(elements.recipe);

       //Create new recipe object 
        state.recipe = new Recipe(id);
        try {
            //Get recipe data and parse ingredients
            await state.recipe.getRecipe();

            state.recipe.parseIngredients();

        //Calculate servings and time
            state.recipe.calcTime();
            state.recipe.calcServings();

        //Render recipe
            clearLoader();
            recipeView.renderRecipe(state.recipe);
        } catch (error) {
           console.log(error);
        }
    }
}

['hashchange','load'].forEach(event => window.addEventListener(event,controlRecipe));