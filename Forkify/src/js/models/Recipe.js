import axios from 'axios';
import {key,proxy} from '../config';


export default class Recipe {
    constructor (id) {
        this.id = id;
    }

    async getRecipe (){

        try {
            const res = await axios(`${proxy}https://www.food2fork.com/api/get?key=${key}&rId=${this.id}`);
            console.log(res);
            this.title = res.data.recipe.title;
            this.author =  res.data.recipe.publisher;
            this.img = res.data.recipe.image_url;
            this.url = res.data.recipe.source_url;
            this.ingredients = res.data.recipe.ingredients;
        }
        catch (error){
            console.log (error);
        }
    }

    calcTime () {
        const numIngredients = this.ingredients.length;
        const periods = Math.ceil (numIngredients/3);
        this.time =  periods*15;
    }

    calcServings () {
        this.servings = 4;
    }

    parseIngredients () {
        const unitsLong = ['tablespoons','tablespoon','onces','once','teaspoons', 'teaspoon','cups','pounds'];
        const unitsShort = ['tbsp','tbsp','oz','oz','tsp','tsp', 'cup','pound'];


        const newIngredients = this.ingredients.map(el =>{
            // uniform units
            let ingredient = el.toLowerCase();
            unitsLong.forEach((unit, i) => {
                ingredient = ingredient.replace (unit, unitsShort[i]);
            });

            // remove parenteses
            ingredient = ingredient.replace (/ *\([^)]*\) */g, ' ');

            // parce ingredients into 'count', 'units' and 'ingredient'
            const arrIng = ingredient.split(' ');
            const unitIndex = arrIng.findIndex(el2 => unitsShort.includes(el2));
            //good so far
            let objIng;

            if (unitIndex > -1) {
                //there is unit
                let count;
                const arrCount = arrIng.slice(0, unitIndex);
                if(arrCount.length === 1) {
                    count = eval(arrIng[0].replace('-','+'));
                } else {
                    count = eval(arrIng.slice(0,unitIndex).join('+'));
                }
                objIng = {
                    count,
                    unit : arrIng[unitIndex],
                    ingredient: arrIng.slice(unitIndex+1).join(' ')
                }
            }else if (parseInt(arrIng[0],10)){
                objIng = {
                    count:parseInt(arrIng[0],10),
                    unit : '',
                    ingredient:arrIng.slice(1).join(' ')
                }
            } else if (unitIndex === -1) {
                //no unit and no number in first array element
                objIng = {
                    count:1,
                    unit : '',
                    ingredient
                }
            }
            return objIng;
        });
        this.ingredients = newIngredients;
    }
}

