import { elements } from './base';

export const getInput = () => elements.searchInput.value;

export const clearInput = () => {
    elements.searchInput.value = '';
};  
export const clearResults = () => {
    elements.searchResults.innerHTML = '';  
    elements.searchResultsPages.innerHTML = '';  
};  

const limitRecipeTitle = (title, limit = 17) => {
    const newTitle = [];
    if(title.length > limit) {
        title.split(' ').reduce((accumulator, current) =>{
            if((accumulator + current.length) < limit) {
                newTitle.push(current);
            }
            return accumulator + current.length;
        }, 0);
        return `${newTitle.join(' ')} ... `;
    }
    return title;
};

const renderSingleRecipe = recipe => {
    const htmlElementMarkup = 
        `<li>
            <a class="results__link" href="#${recipe.recipe_id}">
                <figure class="results__fig">
                    <img src="${recipe.image_url}" alt="recipe.title">
                </figure>
                <div class="results__data">
                    <h4 class="results__name">${limitRecipeTitle(recipe.title)}</h4>
                    <p class="results__author">${recipe.publisher}</p>
                </div>
            </a>
        </li>`;
    elements.searchResults.insertAdjacentHTML('beforeend',htmlElementMarkup);
};

const createButton = (page, type) => `
    <button class="btn-inline results__btn--${type}" data-goto=${type === 'prev' ? page - 1 : page + 1}>
        <span>Page ${type === 'prev' ? page - 1 : page + 1}</span>
        <svg class="search__icon">
            <use href="img/icons.svg#icon-triangle-${type === 'prev' ? 'left' : 'right'}"></use>
        </svg>
    </button>
`;

const renderButtons = (page, numOfResults, resultPerPage) => {

    const pages = Math.ceil(numOfResults / resultPerPage);

    let button;
    if (page === 1 && pages > 1) {
        //only next button
        button = createButton(page, 'next');

    } else if (page < pages) {
        // we need both butons
        button = `
            ${createButton(page, 'prev')}
            ${createButton(page, 'next')}
        `;
    } else if (page === pages && pages > 1) {
        // only previous button
        button = createButton(page, 'prev');
    };
    
    elements.searchResultsPages.insertAdjacentHTML('afterbegin', button);
};

export const renderSearchResults = (recipes, page = 1, resultPerPage = 10) => {
    // Render pages
    const start = (page-1) * resultPerPage; //0, 10, 20 the array is 0-based

    const end = page * resultPerPage; //9, 19, 29 slice doesn't read the upper border

    recipes.slice(start,end).forEach(renderSingleRecipe);

    // Shows buttons
    renderButtons(page, recipes.length, resultPerPage);
};