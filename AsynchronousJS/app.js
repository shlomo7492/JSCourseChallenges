const printPercentages = id => {
    setTimeout ( () => {
        document.querySelector('.body').innerHTML = `${id}%`;
        console.log(`--${id}%`)
    }, 1000);
}

const percentages = ()  => {
    let i = 0;
    while( i <= 100) {        
        printPercentages(i++);
    }
} 

// Solving the delete button click event
const closeList = document.querySelectorAll('.demo');
closeList.forEach(element => 
  element.addEventListener('click', (event) => { 
        let value = event.target.value;
        let type = value.substr(0,3).toUpperCase();
        let id = value.substr(3);
        alert (`Type of the entry is ${type} and it has an ID = ${id}`);
    }));

percentages ();