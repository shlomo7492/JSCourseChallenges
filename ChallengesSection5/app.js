(function (){
    /*Build an using a constructor function
    object for question wiht question itself as a property, and the answers 2 - 3, 
    also the correct answer */
    var question1, question2, question3, question4, question5, question6,
        shownQuestions,checkedAnswers;
    //will store the answers here in format questionQuestionNumber : answer value (exmpl: question1:1).
    var Answer = function (body,correct){
        this.body = body;
        this.correct= correct;
    };


    function Question(question,answer1,answer2,answer3){
        this.question = question;
        this.answersArray = [answer1,answer2,answer3];
    }
    question1 = new Question('Who is Michael Jordan?',
        new Answer ('The other name of Bugs Bunny',false),
        new Answer ('Who?',false),
        new Answer ('Do you mean \'His Airness\'', true));

    question2 = new Question ('Which of these was a real name of Plovdiv?',
        new Answer ('Maina Town',false),
        new Answer ('Philipolis',true),
        new Answer ('Town of the Hills',false));
    question3 = new Question ('Who are the best mentors in Programista Internship program?',
        new Answer ('Tony and Kiko',false),
        new Answer ('Ani and Tony',true),
        new Answer ('Preslava and Fiki',false));
    question4 = new Question ('How many frontenders are there in the Inernship program?',
        new Answer ('Four.',true),
        new Answer ('How many do you want them to be? Lets money speak.',false),
        new Answer ('What is frontend',false));
    question5 = new Question ('Is it cold outside?',
        new Answer ('Sure it is.',true),
        new Answer ('Who cares, I am not going out anyway.',false),
        new Answer ('Hmm... Where do you download \'Outside\' from?',false));
    question6 = new Question ('Daddy may I play on your smart phone, pls?',
        new Answer ('Make 150 push-ups and we are good.',false),
        new Answer ('No way!', true),
        new Answer ('Of course, if you pay me 5 bugs',false));
    
        // ALL OF THE FUNCTIONS DOWNWARD TO BE REWRITTEN, TO DEFRAGMENT THE CODE TO ALL SMALL TASKS
    function init (){
        event.preventDefault();
        /*stores  numbers from 1to 6 for all the question selected randomly and is used 
        to prevent repetition of the questions also in the end helps reveal the right answers*/
        shownQuestions = [];
        checkedAnswers = [];
        document.getElementById('submit').style.display = 'block';
        document.getElementById('new-init').style.display = 'none';
        document.getElementById('ans-1').style.display = 'inline-block';
        document.getElementById('ans-2').style.display = 'inline-block';
        document.getElementById('ans-3').style.display = 'inline-block';
        document.getElementById('new-quiz').style.display = 'none';
        document.getElementById('results').style.display = 'none';
        selectRandomQuestion ();
    }

    function uncheck(){
        for(var i = 1; i <= 3; i++) {
            if(checkedRadio('ans-' + i)){
                checkedAnswers.push (checkedRaioValue('ans-' + i));
                document.getElementById('ans-' + i).checked = false; 
                console.log('ans-' + i + ' unchecked, value of: ' + checkedRaioValue('ans-' + i) );
                document.getElementById('lab-' + i).classList.remove('label-chosen'); 
                return;
            }
        }
    }
    function checkedRadio (elementId){
        return document.getElementById(elementId).checked;
    }

    //returns the value of the checked element and ads it to
    function checkedRaioValue (elementId) {
        return parseInt(document.getElementById(elementId).value);
    }
    //this shows the alert and "Please, check one of the answers to continue!" and
    //prevents submiting.
    function preventEmptySubmut(fn) {
        var result;
        for(var i = 1; i <= 3; i++) {
            result = fn('ans-' + i);
            console.log('ans-' + i + '-' + result);
            if(result === true) return result;
        }
        if (shownQuestions.length > 0 && shownQuestions.length <= 6){
            event.preventDefault();
            alert("Please, check one of the answers to continue!");
        }
        return result;
    }
    function getCorrect(arr) {
        for (var i = 0; i < arr.length;i++) {
            if(arr[i].correct === true){
                return arr[i].body;
            }
        }
    }
    function showSummary (fn,fn1) {
        document.getElementById('results').style.display = 'block';
        var summaryText = ' ';
        for (var i = 0; i < shownQuestions.length; i++) {
            var question = fn(shownQuestions[i]).question;
            var ansArr = fn(shownQuestions[i]).answersArray;
            var yourAns = ansArr[checkedAnswers[i]].body;
            correctAns =  fn1(ansArr);
            summaryText += '<p><strong><span class="red">Q' + (i+1) + '.</span> ' + question + '</strong></p> <p><strong>Correct answer is:</strong> ' 
            + correctAns + '</p><p><strong>Your answer is:</strong> ' + yourAns + '</p>';
        }
        document.getElementById('summary').innerHTML =summaryText;
    }
    //finaly shows result calls summry() as well
    function showResults () {
        uncheck();  
        document.getElementById('submit').style.display = 'none';
        document.getElementById('new-quiz').style.display = 'block';
        document.getElementById('lab-2').classList.remove('label-chosen');
        document.getElementById('lab-1').classList.remove('label-chosen');
        document.getElementById('lab-3').classList.remove('label-chosen');
        showSummary (choseQuestion,getCorrect);
    }

    //Makes shure there are not dublicate questions
    function randomSelect () {
        var choice = 0;

        while (choice === 0){
            var choice = Math.floor(Math.random() * 6 +1);
            if(shownQuestions.length === 0){
                break;
            }
            for (var i = 0; i < shownQuestions.length; i++){
                if(choice === shownQuestions[i]){
                    choice = 0;
                }
            }
        }
        return choice;
    }
    function choseQuestion (choice) {
        switch (choice) {
            case 1: {
                return question1; 
            }
            case 2:{
                return question2; 
            }
            case 3:{
                return question3; 
            }
            case 4:{
                return question4;
            }
            case 5:{
                return question5;
            }
            case 6:{
                return question6;
            }
            default:break;
        }
    }

    function selectRandomQuestion (){
        if(shownQuestions.length === 6){
            showResults();
            return;
        }
    
        var choice = randomSelect();
        console.log(choice);
        if (((choice > 0 ) && (preventEmptySubmut(checkedRadio))) ||
            ((choice > 0) && (shownQuestions.length === 0))) {
                
            showQuestion(choseQuestion(choice)); 
                
            shownQuestions.push(choice);

            if(shownQuestions.length <= 6) {
                uncheck();
            }
        }
    }
    function showQuestion(fn){
        console.log (fn);
        var currentQuestion = fn;
        document.getElementById('question').textContent =(shownQuestions.length+1) + '. ' + currentQuestion.question;
        document.getElementById('lab-1').textContent = currentQuestion.answersArray[0].body;
        document.getElementById('lab-2').textContent = currentQuestion.answersArray[1].body;
        document.getElementById('lab-3').textContent = currentQuestion.answersArray[2].body;
    }

    document.getElementById('submit').addEventListener('click', function(event) {
        event.preventDefault();
        //updates the checkedAnswers[] with the value of the checked radio btn 
        // if it is the last question, shows the summary, hide itself and shows the new-quiz btn
        console.log(shownQuestions);
        console.log(checkedAnswers);

        //updates the question, and all three input radio btns with new ones. clears all checks

        selectRandomQuestion();
    });

    document.getElementById('new-quiz').addEventListener('click', init);
    document.getElementById('new-init').addEventListener('click', init);

    document.getElementById('lab-1').addEventListener('click',function(){
        checkedAnswers.length < 6 ? 
                document.getElementById('lab-1').classList.add('label-chosen'):
                document.getElementById('lab-1').classList.remove('label-chosen');
        document.getElementById('lab-2').classList.remove('label-chosen');
        document.getElementById('lab-3').classList.remove('label-chosen');
    });
    document.getElementById('lab-2').addEventListener('click',function(){
        checkedAnswers.length < 6 ? 
                document.getElementById('lab-2').classList.add('label-chosen'):
                document.getElementById('lab-2').classList.remove('label-chosen');
        document.getElementById('lab-1').classList.remove('label-chosen');
        document.getElementById('lab-3').classList.remove('label-chosen');
    });
    document.getElementById('lab-3').addEventListener('click',function(){
        checkedAnswers.length < 6 ? 
            document.getElementById('lab-3').classList.add('label-chosen'):
            document.getElementById('lab-3').classList.remove('label-chosen');    
        document.getElementById('lab-2').classList.remove('label-chosen');
        document.getElementById('lab-1').classList.remove('label-chosen');
    });
}) ();