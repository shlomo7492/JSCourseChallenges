/***************************************/
/********** FIRST CHALLENGE ************/
/***************************************/
console.log('FIRST CHALLENGE');
//IMPORTANT: All measurments are in metric units only!
var weightJohn = 85;
var weightMark = 78;
var heightJohn = 1.77;
var heightMark = 1.82;

var bmiMark,bmiJohn;
bmiJohn = weightJohn / (heightJohn * heightJohn);
bmiMark = weightMark / (heightMark * heightMark);

var isMarkRounderThanJohn = bmiMark > bmiJohn;

console.log ("John's BMI: " + bmiJohn);
console.log ("Mark's BMI: " + bmiMark);

console.log("Is Mark's BMI higher than John's?" + ' ' + isMarkRounderThanJohn);

console.log('Is Mark\'s BMI higher than John\'s?' +
             ' ' + isMarkRounderThanJohn +' \( with single quotes and escaping\)');

/***************************************/
/********** SECOND CHALLENGE ************/
/***************************************/
console.log('SECOND CHALLENGE');
var namePlayer1, averageScorePlayer1,
namePlayer2, averageScorePlayer2, 
namePlayer3, averageScorePlayer3, 
game1, game2, game3;

namePlayer1 = 'John';
game1 = 89;
game2 = 120;
game3 = 103;
averageScorePlayer1=(game1+game2+game3)/3; 

namePlayer2 = 'Mark';
game1 = 116;
game2 = 94;
game3 = 123;
/**** Uncoment the block bellow to check the draw case****/
/* game1 = 89;
game2 = 120;
game3 = 103; */
averageScorePlayer2=(game1+game2+game3)/3; 

namePlayer3 = 'Mary';
game1 = 97;
game2 = 134;
game3 = 105;
/**** Uncoment the block bellow to check the draw case****/
/* game1 = 89;
game2 = 120;
game3 = 103; */
averageScorePlayer3=(game1+game2+game3)/3; 

if(averageScorePlayer1>averageScorePlayer2){
    console.log(namePlayer1 + ' has a higher average score than ' +
                 namePlayer2 + " and it is: " +averageScorePlayer1);
}
else if(averageScorePlayer1<averageScorePlayer2){
    console.log(namePlayer2 + ' has a higher average score than ' +
                 namePlayer1 + " and it is: " +averageScorePlayer2);
} else{
    console.log('Both ' + namePlayer1 + ' and ' +
                 namePlayer2 + " got equal average score of " +averageScorePlayer1);
}

/**** Include Mary in the comparison ****/

if(averageScorePlayer1>averageScorePlayer2){
    if (averageScorePlayer1>averageScorePlayer3){
        console.log (namePlayer1 + ' is the winner with average score of ' +
         averageScorePlayer1);
    } else if(averageScorePlayer1===averageScorePlayer3){
        console.log ('Both ' + namePlayer1 + ' and ' +
        namePlayer3 + " are the winners with equal average score of " + 
        averageScorePlayer1);
    } else {
        console.log (namePlayer3 + ' is the winner with average score of ' +
         averageScorePlayer3);
    }
}
else if (averageScorePlayer1<averageScorePlayer2){
   
    if (averageScorePlayer2>averageScorePlayer3){
        console.log (namePlayer2 + ' is the winner with average score of ' +
         averageScorePlayer2);
    } else if(averageScorePlayer2===averageScorePlayer3){
        console.log ('Both ' + namePlayer2 + ' and ' +
        namePlayer3 + " are the winners with equal average score of " + 
        averageScorePlayer2);
    } else {
        console.log (namePlayer3 + ' is the winner with average score of ' +
         averageScorePlayer3);
    }
} else  if (averageScorePlayer2>averageScorePlayer3){
    console.log ('Both ' + namePlayer1 + ' and ' +
    namePlayer2 + " are the winners with equal average score of " + 
    averageScorePlayer1);
} else if(averageScorePlayer2===averageScorePlayer3){
    console.log ('All player are the winners with equal average score of ' + 
    averageScorePlayer2 + '. No one lose.');
} else {
    console.log (namePlayer3 + ' is the winner with average score of ' +
    averageScorePlayer3);
}

/***************************************/
/********** THIRD CHALLENGE ************/
/***************************************/
console.log('THIRD CHALLENGE');
//Arrays and Functions basics

var restaurantOne,restaurantTwo,restaurantThree;
restaurantOne = 124;
restaurantTwo = 48;
restaurantThree = 268;
var bills =[restaurantOne,restaurantTwo,restaurantThree];
var tips = [];

function tipCalculator(bill){
    switch (bill){
        case bill <= 50 : {
            return bill * 0.2;   
        }
        case bill<=200 : {
            return bill * 0.15;   
        }
        default: return bill * 0.1;
    }
}
/*now a stolen function 'format(num)' to format the outut numbers 
with two digits after the floating point 
(little midification to fit our purpouse)*/
function format( num ){
    return ( Math.floor(num * 100)/100 )  // slice decimal digits after the 2nd one
    .toFixed(2)  // format with two decimal places
    .replace(/\.$/,''); // remove trailing decimal place separator
}


for (var i=0;i<bills.length;i++){
    tips[i] = tipCalculator(bills[i]);
    bills[i] += tips[i]; 
    console.log ('In resaurant ' + (i+1) + ' John paid ' 
    + format(tips[i]) + ' dolar tip and paid in total ' 
    + format(bills[i]) + ' dolars.');
}

/***************************************/
/********** FOURTH CHALLENGE ************/
/***************************************/
console.log('FOURTH CHALLENGE');
/* var john = {
    firstName: 'John',
    lastName: 'Smith',
    birtYear: 1990,
    calcAge: function(){
        var d = new Date();
        return d.getFullYear() - this.birtYear;
    },
}; */
var person = {
    firstName : '',
    lastName : '',
    mass : 0,
    height : 0,
    person : function (firstName,lastName,mass,height) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.mass = mass;
        this.height = height;
        this.bmi = format (this.mass/ (this.height*this.height));   
    },
    
};
//Change the numeric values to see how the comparison is working
   /*  var john = new person.person('John','Smith',78,1.78);
    var mark = new person.person('Mark','Adams',78,1.78);
    

    console.log(john);
    console.log(mark);
    if(john.bmi>mark.bmi){
        console.log(john.firstName + ' ' +john.lastName + ' has higher BMI of' + john.bmi);
    } else if(john.bmi<mark.bmi) {
        console.log(mark.firstName + ' ' +mark.lastName + ' has higher BMI of' + mark.bmi);
    } else {
        console.log(john.firstName + ' ' +john.lastName + ' and ' + mark.firstName + ' ' +mark.lastName + ' got equal BMI of ' + john.bmi);
    }
 */
/***************************************/
/********** FIFTH CHALLENGE ************/
/***************************************/
console.log('FIFTH CHALLENGE');

function resaurantBills(name){
    this.nameOfPayer = name;
    this.bills = [];
    this.tips = [];
    this.totalBills = [];
    this.tipAndTotalCalculator = function(){
        for (var i = 0; i < this.bills.length; i++){
            if(this.bills[i] <=50){
                this.tips[i]= parseFloat(format(this.bills[i]*0.2));
            } else if (this.bills[i]<=200){
                this.tips[i]= parseFloat(format(this.bills[i]*0.15));
            } else {
                this.tips[i]= parseFloat(format(this.bills[i]*0.1));
            }
            this.totalBills[i] = this.bills[i] + this.tips[i];
        }
    }
} 
var johny = new resaurantBills('John');
var marko = new resaurantBills('Mark');

johny.bills = [124,48,268,180,42];
johny.tipAndTotalCalculator();

marko.bills = [77,375,110,45];
marko.tipAndTotalCalculator = function(){
    for (var i = 0; i < this.bills.length; i++){
        if(this.bills[i] <=100){
            this.tips[i]= parseFloat(format(this.bills[i]*0.2));
        } else if (this.bills[i]<=300){
            this.tips[i]= parseFloat(format(this.bills[i]*0.1));
        } else {
            this.tips[i]= parseFloat(format(this.bills[i]*0.25));
        }
        this.totalBills[i] = this.bills[i] + this.tips[i];
    }
};
marko.tipAndTotalCalculator();
console.log(johny.nameOfPayer + ':');
console.log(johny.bills);
console.log(johny.tips);
console.log(johny.totalBills);
console.log('-*-*-*-*-*-*-*-*-');
console.log(marko.nameOfPayer + ':');
console.log(marko.bills);
console.log(marko.tips);
console.log(marko.totalBills);